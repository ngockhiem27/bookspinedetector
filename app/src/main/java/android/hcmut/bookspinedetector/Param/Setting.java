package android.hcmut.bookspinedetector.Param;

import org.opencv.core.Point;
import org.opencv.core.Mat;

/**
 * Created by kiddy_000 on 14-Apr-16.
 */
public class Setting {
    public static String MIN_CANNY_THRESHOLD = "50";
    public static String MAX_CANNY_THRESHOLD = "120";
    public static String SOBEL_THRESHOLD = "254";
    public static Mat ZOOM_IMAGE;
    public static StandardLine standardLine;
    public static double MIN_LINE_RATIO = 0.35;

    public static final String CANNY_MODE = "CANNY";
    public static final String SOBEL_MODE = "SOBEL";

    public static final String TOP_ROI = "TOP";
    public static final String RIGHT_ROI = "RIGHT";
    public static final String BOTTOM_ROI = "BOTTOM";
    public static final String LEFT_ROI = "LEFT";

    public static class StandardLine {
        public org.opencv.core.Point point1;
        public org.opencv.core.Point point2;

        public StandardLine(Point point1, Point point2) {
            this.point2 = point2;
            this.point1 = point1;
        }
    }


    public static void setStandardLine(Mat mat) {
        standardLine = new StandardLine(new Point(mat.cols() / 2, 0), new Point(mat.cols() / 2, mat.rows()));
    }
}
