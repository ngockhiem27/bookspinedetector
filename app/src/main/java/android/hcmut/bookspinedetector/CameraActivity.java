package android.hcmut.bookspinedetector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hcmut.bookspinedetector.Param.Setting;
import android.hcmut.bookspinedetector.Process.Detection;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraActivity extends Activity implements CvCameraViewListener2, OnTouchListener {
    private static final String TAG = "CameraActivity:";

    private CameraView mOpenCvCameraView;
    private Mat mRgba;
    private Mat mRgbaLine;
    private Mat mat_final;
    private boolean shape_mode = false;
    Button btnShot;
    Button btnEdge;
    Button btnReshot;
    ImageView imgShot;
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(CameraActivity.this);
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.camera_preview);

        mOpenCvCameraView = (CameraView) findViewById(R.id.java_camera_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setFocusable(true);
        mOpenCvCameraView.setOnTouchListener(CameraActivity.this);

        btnShot = (Button) findViewById(R.id.btn_shot);
        btnShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long startTime = System.nanoTime();

                Core.transpose(mRgba, mat_final);
                Core.flip(mat_final, mat_final, 1);
                Setting.setStandardLine(mat_final);
                Mat result = Detection.composeProcess(mat_final);

                long endTime = System.nanoTime();
                long duration = (endTime - startTime)/1000000;

                if (result == null) {
                    Toast.makeText(CameraActivity.this, "Cant found any book", Toast.LENGTH_SHORT).show();
                } else {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                    String currentDateandTime = sdf.format(new Date());
                    String fileName = Environment.getExternalStorageDirectory().getPath() +
                            "/sample_picture_" + currentDateandTime + ".jpg";
                    mOpenCvCameraView.takePicture(fileName);

                    showImageToScreen(result);

                    btnShot.setEnabled(false);
                    btnEdge.setEnabled(false);
                    btnReshot.setEnabled(true);
                    mOpenCvCameraView.setVisibility(View.GONE);
                    imgShot.setVisibility(View.VISIBLE);
                    btnShot.setVisibility(View.GONE);
                    btnEdge.setVisibility(View.GONE);
                    btnReshot.setVisibility(View.VISIBLE);
                    Toast.makeText(CameraActivity.this, "Calculation time " + String.valueOf(duration) + " ms", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnEdge = (Button) findViewById(R.id.btn_edge);
        btnEdge.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        shape_mode = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        shape_mode = false;
                        break;
                }
                return true;
            }
        });

        btnReshot = (Button) findViewById(R.id.btn_reshot);
        btnReshot.setVisibility(View.GONE);
        btnReshot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnShot.setEnabled(true);
                btnEdge.setEnabled(true);
                btnReshot.setEnabled(false);
                mOpenCvCameraView.setVisibility(View.VISIBLE);
                imgShot.setVisibility(View.GONE);
                btnShot.setVisibility(View.VISIBLE);
                btnEdge.setVisibility(View.VISIBLE);
                btnReshot.setVisibility(View.GONE);
            }
        });
        imgShot = (ImageView) findViewById(R.id.image_shot);
        imgShot.setVisibility(View.GONE);
    }

    private void showImageToScreen(Mat mat) {

        Bitmap bmp = Bitmap.createBitmap(mat.width(), mat.height(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(mat, bmp);

        imgShot.setImageBitmap(bmp);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found inside . Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mOpenCvCameraView.setFocusMode(this, Camera.Parameters.FOCUS_MODE_AUTO);
        mOpenCvCameraView.setFlashMode(this, Camera.Parameters.FLASH_MODE_ON);
        mRgba = new Mat(height, width, CvType.CV_8UC3);
        mRgbaLine = new Mat(height, width, CvType.CV_8UC3);
        mat_final = new Mat(height, width, CvType.CV_8UC3);
    }

    public void onCameraViewStopped() {
        mRgba.release();
        mRgbaLine.release();
        mat_final.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        // width: 800 | height: 480
        inputFrame.rgba().copyTo(mRgba);
        inputFrame.rgba().copyTo(mRgbaLine);
        Imgproc.line(mRgbaLine, new Point(0, mRgbaLine.height() / 2), new Point(mRgbaLine.width(), mRgbaLine.height() / 2), new Scalar(255, 0, 0), 1);
        if (shape_mode) {
            return Detection.shapeProcess(mRgbaLine);
        }
        return mRgbaLine;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
//        Log.i(TAG, "onTouch event");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
//        String currentDateandTime = sdf.format(new Date());
//        String fileName = Environment.getExternalStorageDirectory().getPath() +
//                "/sample_picture_" + currentDateandTime + ".jpg";
//        mOpenCvCameraView.takePicture(fileName);
//        Toast.makeText(this, fileName + " saved", Toast.LENGTH_SHORT).show();
        mOpenCvCameraView.focusOnTouch(event);
        return false;
    }
}