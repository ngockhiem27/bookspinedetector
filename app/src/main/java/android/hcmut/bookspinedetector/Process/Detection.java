package android.hcmut.bookspinedetector.Process;

import android.hcmut.bookspinedetector.Param.Setting;
import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by kiddy_000 on 13-Apr-16.
 */
public class Detection {

    public static final String TAG = Detection.class.getName();

    public static Mat grayscaleMat(Mat mat) {
        Mat result = new Mat();
        Imgproc.cvtColor(mat, result, Imgproc.COLOR_RGBA2GRAY);

        return result;
    }

    public static Mat enhanceMat(Mat mat) {
        Mat result = new Mat();
        Imgproc.equalizeHist(mat, result);

        return result;
    }

    public static Mat edgeMat(Mat mat) {
        Mat result = new Mat();

        int threshold1 = Integer.parseInt(Setting.MIN_CANNY_THRESHOLD);
        int threshold2 = Integer.parseInt(Setting.MAX_CANNY_THRESHOLD);
        Imgproc.Canny(mat, result, threshold1, threshold2);

        return result;
    }

    public static Mat blurMat(Mat mat) {
        Mat result = new Mat();
        Imgproc.GaussianBlur(mat, result, new Size(3, 3), 0);

        return result;
    }

    public static Mat shapeMat(Mat mat) {
        List<MatOfPoint> contourList = new ArrayList<>();
        Mat hierachy = new Mat();
        Mat result = new Mat();

        Imgproc.findContours(mat, contourList, hierachy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);
        result.create(mat.rows(), mat.cols(), CvType.CV_8UC3);

        Random random = new Random();

        for (int i = 0; i < contourList.size(); i++) {
            Scalar color = new Scalar(random.nextInt(255), random.nextInt(255), random.nextInt(255));
            int lenth = contourList.get(i).toList().size();
            if (lenth > 100) {
                Imgproc.drawContours(result, contourList, i, color, -1);
            }
        }

        return result;
    }

    public static Mat sobelMat(Mat mat) {
        Mat result = new Mat();
        Mat dx = new Mat();
        Mat dy = new Mat();
        Mat abs_dx = new Mat();
        Mat abs_dy = new Mat();

        Imgproc.Sobel(mat, dx, CvType.CV_16S, 1, 0);
        Imgproc.Sobel(mat, dy, CvType.CV_16S, 0, 1);

        Core.convertScaleAbs(dx, abs_dx);
        Core.convertScaleAbs(dy, abs_dy);

        // hieu change 0.5 -> 2.8, 0.5 -> 2.8
        Core.addWeighted(abs_dx, 2.8, abs_dy, 2.8, 0, result);

        Imgproc.threshold(result, result, Integer.parseInt(Setting.SOBEL_THRESHOLD), 255, Imgproc.THRESH_BINARY);

        return result;
    }

    public static Mat houghMat(Mat edgeMat) {
        Mat lines = new Mat();
        // hieu change 50 -> 10, 50 -> 100
        Imgproc.HoughLinesP(edgeMat, lines, 1, Math.PI / 180, 10, 150, 5);
        return lines;
    }

    public static void calcHistogram(Mat image) {
        int mHistSizeNum = 25;
        MatOfInt mHistSize = new MatOfInt(mHistSizeNum);
        Mat hist = new Mat();
        float[] mBuff = new float[mHistSizeNum];
        MatOfFloat histogramRanges = new MatOfFloat(0f, 256f);
        Scalar mColorsRGB[] = new Scalar[]{new Scalar(200, 0, 0, 255), new
                Scalar(0, 200, 0, 255), new Scalar(0, 0, 200, 255)};
        org.opencv.core.Point mP1 = new org.opencv.core.Point();
        org.opencv.core.Point mP2 = new org.opencv.core.Point();
        int thikness = (int) (image.width() / (mHistSizeNum + 10) / 3);
        if (thikness > 3) thikness = 3;
        MatOfInt mChannels[] = new MatOfInt[]{new MatOfInt(0), new MatOfInt(1),
                new MatOfInt(2)};
        Size sizeRgba = image.size();
        int offset = (int) ((sizeRgba.width - (3 * mHistSizeNum + 30) * thikness));
        // RGB
        for (int c = 0; c < 3; c++) {
            Imgproc.calcHist(Arrays.asList(image), mChannels[c], new Mat(), hist,
                    mHistSize, histogramRanges);
            Core.normalize(hist, hist, sizeRgba.height / 2, 0, Core.NORM_INF);
            hist.get(0, 0, mBuff);
            for (int h = 0; h < mHistSizeNum; h++) {
                mP1.x = mP2.x = offset + (c * (mHistSizeNum + 10) + h) * thikness;
                mP1.y = sizeRgba.height - 1;
                mP2.y = mP1.y - (int) mBuff[h];
                Imgproc.line(image, mP1, mP2, mColorsRGB[c], thikness);
            }
        }
    }

    /**
     * Khiem version
     */
    public static Mat detectFromHough(Mat houghMat, Mat originalMat) {
        Mat draw = new Mat();

        //Drawing lines on the image
        List<Point> right = new ArrayList<>();
        List<Point> left = new ArrayList<>();
        double minRight = 999999;
        double minLeft = -999999;
        double minLineAccepted = originalMat.height() * Setting.MIN_LINE_RATIO;
        Log.d(" houghmat rows", String.valueOf(houghMat.rows()));
        if (houghMat.rows() == 0){
            return null;
        }
        for (int i = 0; i < houghMat.rows(); i++) {
            double[] points = houghMat.get(i, 0);
            double x1, y1, x2, y2;
            x1 = points[0];
            y1 = points[1];
            x2 = points[2];
            y2 = points[3];
            Point pt1 = new Point(x1, y1);
            Point pt2 = new Point(x2, y2);
            double distance = Math.abs(y2 - y1);
            // hieu change 300.0 -> 350.0
            if (distance >= 350) {
                double disFromStandard = pt1.x - Setting.standardLine.point1.x;
                if (disFromStandard > 0 && disFromStandard < minRight) {
                    minRight = disFromStandard;
                    if (!right.isEmpty()) right.clear();
                    right.add(pt1);
                    right.add(pt2);
                } else if (disFromStandard < 0 && disFromStandard > minLeft) {
                    minLeft = disFromStandard;
                    if (!left.isEmpty()) left.clear();
                    left.add(pt1);
                    left.add(pt2);
                }

            }
        }
        if (left.isEmpty() || right.isEmpty()){
            return null;
        }
        originalMat.copyTo(draw);
        //Drawing lines on an image
        for (int i = 0; right.size() > i; i += 2) {
            Imgproc.line(draw, right.get(i), right.get(i + 1), new Scalar(0, 0, 255), 4);
        }
        for (int i = 0; left.size() > i; i += 2) {
            Imgproc.line(draw, left.get(i), left.get(i + 1), new Scalar(0, 255, 0), 4);
        }
        HashMap<String, Integer> ROICoordinates = findROICoordinates(left, right, draw);

        return draw.submat(ROICoordinates.get(Setting.TOP_ROI), ROICoordinates.get(Setting.BOTTOM_ROI), ROICoordinates.get(Setting.LEFT_ROI), ROICoordinates.get(Setting.RIGHT_ROI));
        //return draw.submat(0, draw.rows(), left.get(0).x < left.get(1).x ? (int) left.get(0).x : (int) left.get(1).x, right.get(0).x > right.get(1).x ? (int) right.get(0).x : (int) right.get(1).x);
    }

    private static HashMap<String, Integer> findROICoordinates(List<Point> leftLine, List<Point> rightLine, Mat inputMat) {
        HashMap<String, Integer> result = new HashMap<>();
        int top = 0;
        int bottom = inputMat.height();
        result.put(Setting.TOP_ROI, top);
        result.put(Setting.BOTTOM_ROI, bottom);

        int left = (int) findLeftOrRight(leftLine, top, bottom, Setting.LEFT_ROI);
        int right = (int) findLeftOrRight(rightLine, top, bottom, Setting.RIGHT_ROI);

        if (left > right) {
            int temp = right;
            right = left;
            left = temp;
        }
        result.put(Setting.LEFT_ROI, left);
        result.put(Setting.RIGHT_ROI, right);

        return result;
    }

    private static double findLeftOrRight(List<Point> line, double top, double bottom, String MODE) {
        double x1 = line.get(0).x;
        double y1 = line.get(0).y;
        double x2 = line.get(1).x;
        double y2 = line.get(1).y;

        if (Math.abs(x1 - x2) <= 5.0) {
            return x1;
        }

        double a = (y2 - y1) / (x2 - x1);
        double b = y1 - a * x1;

        double xTop = (top - b) / a;
        double xBottom = (bottom - b) / a;

        switch (MODE) {
            case Setting.LEFT_ROI:
                return (xTop < xBottom) ? xTop : xBottom;
            case Setting.RIGHT_ROI:
                return (xTop > xBottom) ? xTop : xBottom;
            default:
                return 0.0;
        }
    }

    private static double findDistanceBetweenTwoLines(List<Point> line1, List<Point> line2, Mat mat) {
        int top = 0;
        int bottom = mat.height();

        double x1 = findLeftOrRight(line1, top, bottom, Setting.LEFT_ROI);
        double x2 = findLeftOrRight(line2, top, bottom, Setting.RIGHT_ROI);

        return Math.abs(x2 - x1);
    }

    public static Mat composeProcess(Mat originalMat){
        Mat blurMat = blurMat(originalMat);
        Mat grayScaleMat = grayscaleMat(blurMat);
        Mat enhanceMat = enhanceMat(grayScaleMat);
        Mat sobelMat = sobelMat(enhanceMat);
        Mat houghLines = houghMat(sobelMat);
        Mat result = detectFromHough(houghLines, originalMat);
        return result;
    }

    public static Mat shapeProcess(Mat mat)  {
        Mat blurMat = blurMat(mat);
        Mat grayScaleMat = grayscaleMat(blurMat);
        Mat enhanceMat = enhanceMat(grayScaleMat);
        Mat sobelMat = sobelMat(enhanceMat);
        return sobelMat;
    }
}