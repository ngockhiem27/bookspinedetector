package android.hcmut.bookspinedetector;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.hcmut.bookspinedetector.Param.Setting;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

/**
 * Created by This PC on 18/04/2016.
 */
public class ImageZoom extends AppCompatActivity implements View.OnTouchListener {
    private static final String TAG="Touch";
    private static final float MIN_ZOOM=1f, MAX_ZOOM=1f;

    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // 3 events
    static  final int NONE=0;
    static final int DRAG=1;
    static final int ZOOM=2;
    int mode= NONE;

    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imagezoom);
        ImageView view =  (ImageView) findViewById(R.id.imageView);

        // initialize image view
        Mat img = Setting.ZOOM_IMAGE;
        Bitmap bmp = Bitmap.createBitmap(img.width(), img.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img, bmp);

        view.setImageBitmap(bmp);

        view.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ImageView view = (ImageView) v;
        view.setScaleType(ImageView.ScaleType.MATRIX);
        float scale;

        dumpEvent(event);

        switch (event.getAction() & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_DOWN:
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                Log.d(TAG,"mode=DRAG");
                mode=DRAG;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode=NONE;
                Log.d(TAG,"mode=NONE");
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist= spacing(event);
                Log.d(TAG,"oldDist= "+oldDist);
                if (oldDist>5f){
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode=ZOOM;
                    Log.d(TAG,"mode=ZOOM");
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(mode==DRAG){
                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX()-start.x,event.getY()-start.y);
                }else if(mode==ZOOM){
                    float newDist= spacing(event);
                    Log.d(TAG,"newDist= "+newDist);
                    if (newDist>5f){
                        matrix.set(savedMatrix);
                        scale= newDist/oldDist;
                        matrix.postScale(scale,scale,mid.x,mid.y);
                    }
                }
                break;
        }
        view.setImageMatrix(matrix);
        return true;
    }

    private float spacing(MotionEvent event){
        float x= event.getX(0)-event.getX(1);
        float y= event.getY(0)-event.getY(1);
        return (float) Math.sqrt((double)(x*x+y*y));
    }

    private  void midPoint(PointF point, MotionEvent event){
        float x= event.getX(0)+event.getX(1);
        float y= event.getY(0)+event.getY(1);
        point.set(x/2,y/2);
    }

    private void dumpEvent(MotionEvent event){
        String names[]={"DOWN","UP","MODE","CANCEL","OUTSIDE","POINTER_DOWN","POINTER_UP","7?","8?","9?"};
        StringBuilder sb = new StringBuilder();
        int action= event.getAction();
        int actionCode= action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);

        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP){
            sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_INDEX_SHIFT).append(")");
        }
        sb.append("[");
        for (int i=0;i<event.getPointerCount();i++){
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i+1 <event.getPointerCount()){
                sb.append(";");
            }
        }

        sb.append("]");
        Log.d("Touch Event",sb.toString());
    }
}
