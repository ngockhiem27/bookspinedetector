package android.hcmut.bookspinedetector;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hcmut.bookspinedetector.Param.Setting;
import android.hcmut.bookspinedetector.Process.Detection;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getName();

    public ImageView showedImage;

    Mat currentMat;
    Mat originalMat, grayscaleMat, enhancedMat, blurMat, edgeMat, shapedMat, resultMat;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("OPENCV LOADED", "OpenCV loaded successfully");
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!OpenCVLoader.initDebug()) {
            Log.i(TAG, "Trying to load OpenCV library");
            if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mOpenCVCallBack))
            {
                Log.e(TAG, "Cannot connect to OpenCV Manager");
            }
            System.loadLibrary("opencv_java3");

        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mOpenCVCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }


        showedImage = (ImageView) findViewById(R.id.image_show);

        Intent intent = getIntent();
        if (intent.getStringExtra("ACTION").equals("PICK_IMG")) {
            pickImage();
            currentMat = originalMat;
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_camera:
                startActivityForResult(new Intent(this, CameraActivity.class), 1);
                break;
            case R.id.action_pick:
                pickImage();
                currentMat = originalMat;
                break;
            case R.id.action_zoom:
                if (currentMat != null) {
                    Intent intent = new Intent(MainActivity.this, ImageZoom.class);
                    Setting.ZOOM_IMAGE = currentMat;
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Please pick one picture by clicking on PICK button", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.action_blur:
                if (enhancedMat != null) {
                    blurMat = Detection.blurMat(enhancedMat);
                    showImageToScreen(blurMat);
                } else if (grayscaleMat != null) {
                    blurMat = Detection.blurMat(grayscaleMat);
                    showImageToScreen(blurMat);
                } else if (originalMat != null) {
                    blurMat = Detection.blurMat(originalMat);
                    showImageToScreen(blurMat);
                } else {
                    Toast.makeText(MainActivity.this, "Please pick one picture by clicking on PICK button", Toast.LENGTH_SHORT).show();
                }
                currentMat = blurMat;
                break;
            case R.id.action_enhance:
                if (grayscaleMat != null) {
                    enhancedMat = Detection.enhanceMat(grayscaleMat);
                    currentMat = enhancedMat;
                    showImageToScreen(enhancedMat);
                } else {
                    Toast.makeText(MainActivity.this, "Please click GRAYSCALE button first", Toast.LENGTH_SHORT).show();
                }
                currentMat = enhancedMat;
                break;
            case R.id.action_grayscale:
                if (originalMat != null) {
                    grayscaleMat = Detection.grayscaleMat(originalMat);
                    currentMat = grayscaleMat;
                    showImageToScreen(grayscaleMat);
                } else {
                    Toast.makeText(MainActivity.this, "Please pick one picture by clicking on PICK button", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.action_canny:
                if (blurMat != null) {
                    edgeMat = Detection.edgeMat(blurMat);
                    showImageToScreen(edgeMat);
                } else if (enhancedMat != null) {
                    edgeMat = Detection.edgeMat(enhancedMat);
                    showImageToScreen(edgeMat);
                } else if (grayscaleMat != null) {
                    edgeMat = Detection.edgeMat(grayscaleMat);
                    showImageToScreen(edgeMat);
                } else {
                    Toast.makeText(MainActivity.this, "Please click GRAYSCALE button first", Toast.LENGTH_SHORT).show();
                }
                currentMat = edgeMat;
                break;
            case R.id.action_contour:
                if (edgeMat != null) {
                    shapedMat = Detection.shapeMat(edgeMat);
                    showImageToScreen(shapedMat);
                } else {
                    Toast.makeText(MainActivity.this, "Please click CANNY OR SOBEL button first", Toast.LENGTH_SHORT).show();
                }
                currentMat = shapedMat;
                break;
            case R.id.action_sobel:
                if (blurMat != null) {
                    edgeMat = Detection.sobelMat(blurMat);
                    showImageToScreen(edgeMat);
                } else if (enhancedMat != null) {
                    edgeMat = Detection.sobelMat(enhancedMat);
                    showImageToScreen(edgeMat);
                } else if (grayscaleMat != null) {
                    edgeMat = Detection.sobelMat(grayscaleMat);
                    showImageToScreen(edgeMat);
                } else {
                    Toast.makeText(MainActivity.this, "Please click GRAYSCALE button first", Toast.LENGTH_SHORT).show();
                }
                currentMat = edgeMat;
                break;
            case R.id.action_hough:
                if (edgeMat != null) {
                    Mat lines = Detection.houghMat(edgeMat);
                    shapedMat = lines;
                    Mat draw = Mat.zeros(edgeMat.rows(), edgeMat.cols(), CvType.CV_8UC1);

                    //Drawing lines on the image
                    for (int j = 0; j < lines.rows(); j++) {
                        for (int i = 0; i < lines.cols(); i++) {
                            double[] points = lines.get(j, i);
                            double x1, y1, x2, y2;
                            x1 = points[0];
                            y1 = points[1];
                            x2 = points[2];
                            y2 = points[3];
                            Point pt1 = new Point(x1, y1);
                            Point pt2 = new Point(x2, y2);
                            //Drawing lines on an image
                            Imgproc.line(draw, pt1, pt2, new Scalar(255, 255, 255), 1);
                        }
                    }
                    showImageToScreen(draw);
                } else {
                    Toast.makeText(MainActivity.this, "Please click CANNY OR SOBEL button first", Toast.LENGTH_SHORT).show();
                }
                currentMat = shapedMat;
                break;
            case R.id.action_getbook:
//                Mat result = Detection.detectFromHough(shapedMat, originalMat);
//                showImageToScreen(result);

                Mat matFinal = new Mat();
                originalMat.copyTo(matFinal);
                Mat result = Detection.composeProcess(matFinal);
                resultMat = result;
                showImageToScreen(result);
                break;
            case R.id.action_feature:
                FeatureDetector detector = FeatureDetector.create(FeatureDetector.SURF );
                MatOfKeyPoint features = new MatOfKeyPoint();
                detector.detect(resultMat, features);
                Log.d("DETECT", String.valueOf(features.toArray().length));
                DescriptorExtractor descriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.SURF);
                Mat des = new Mat();
                descriptorExtractor.compute(resultMat, features, des);
                Log.d("EXTRACT W", String.valueOf(des.size().width));
                Log.d("EXTRACT H", String.valueOf(des.size().height));
//                DescriptorMatcher descriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_SL2);
                break;
            default:
                Intent settingIntent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(settingIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK && data != null) {
            Uri selectedImg = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImg, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            // speed up loading IMG
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap temp = BitmapFactory.decodeFile(picturePath, options);

            //Get orientation information
            int orientation = 0;
            try {
                ExifInterface imgParams = new ExifInterface(picturePath);
                orientation = imgParams.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Rotating img to get correct orientation
            Matrix rotate90 = new Matrix();
            rotate90.postRotate(orientation);
            temp = rotateBitmap(temp, orientation);

            originalMat = null;
            grayscaleMat = null;
            enhancedMat = null;
            edgeMat = null;

            //Convert bitmap to Mat
            //Bitmap tempBitmap = temp.copy(Bitmap.Config.ARGB_8888, true);
            originalMat = new Mat(temp.getHeight(), temp.getWidth(), CvType.CV_8UC4);
            currentMat = new Mat(temp.getHeight(), temp.getWidth(), CvType.CV_8UC4);
            Utils.bitmapToMat(temp, originalMat);
            Utils.bitmapToMat(temp, currentMat);

//            Mat histMat = new Mat();
//            originalMat.copyTo(histMat);
//            Detection.calcHist(histMat);
//            showImageToScreen(histMat);


            Setting.setStandardLine(originalMat);
            Mat centerLine = new Mat(originalMat.rows(), originalMat.cols(), CvType.CV_8UC3);
            originalMat.copyTo(centerLine);
            Imgproc.line(centerLine, Setting.standardLine.point1, Setting.standardLine.point2, new Scalar(255, 255, 0), 4);
            showImageToScreen(centerLine);
        }
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, Uri.parse("content://media/internal/images/media"));
        startActivityForResult(intent, 0);
    }

    private void showImageToScreen(Mat mat) {

        Bitmap bmp = Bitmap.createBitmap(mat.width(), mat.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bmp);
        if (bmp != null) {
            showedImage.setImageBitmap(bmp);
        }
    }

    private static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://android.hcmut.bookspinedetector/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://android.hcmut.bookspinedetector/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
