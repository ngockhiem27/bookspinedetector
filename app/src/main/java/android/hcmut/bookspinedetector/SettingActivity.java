package android.hcmut.bookspinedetector;

import android.hcmut.bookspinedetector.Param.Setting;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;

import java.util.Set;

public class SettingActivity extends AppCompatActivity {
    public RangeBar cannyThresholdBar, sobelThresholdBar;
    public TextView cannyMinThreshold, cannyMaxThreshold, sobelThreshold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cannyMinThreshold = (TextView) findViewById(R.id.canny_min_threshold_tv);
        cannyMaxThreshold = (TextView) findViewById(R.id.canny_max_threshold_tv);
        cannyThresholdBar = (RangeBar) findViewById(R.id.canny_threshold_bar);

        sobelThreshold = (TextView) findViewById(R.id.sobel_threshold_tv);
        sobelThresholdBar = (RangeBar) findViewById(R.id.sobel_threshold_bar);

        initializeValues();

        cannyThresholdBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                Setting.MIN_CANNY_THRESHOLD = leftPinValue;
                Setting.MAX_CANNY_THRESHOLD = rightPinValue;

                adjustThresholdTextView(leftPinValue, rightPinValue, Setting.CANNY_MODE);
            }
        });

        sobelThresholdBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                Setting.SOBEL_THRESHOLD = rightPinValue;

                adjustThresholdTextView(leftPinValue, rightPinValue, Setting.SOBEL_MODE);
            }
        });
    }

    private void initializeValues() {
        cannyMinThreshold.setText("Min threshold = " + Setting.MIN_CANNY_THRESHOLD);
        cannyMaxThreshold.setText("Max threshold = " + Setting.MAX_CANNY_THRESHOLD);
        cannyThresholdBar.setRangePinsByValue(Float.valueOf(Setting.MIN_CANNY_THRESHOLD), Float.valueOf(Setting.MAX_CANNY_THRESHOLD));

        sobelThreshold.setText("Sobel threshold = " + Setting.SOBEL_THRESHOLD);
        sobelThresholdBar.setSeekPinByValue(Float.valueOf(Setting.SOBEL_THRESHOLD));
    }

    private void adjustThresholdTextView(String leftPinValue, String rightPinValue, String type){
        switch (type.toUpperCase()) {
            case Setting.CANNY_MODE:
                cannyMinThreshold.setText("Min threshold = " + leftPinValue);
                cannyMaxThreshold.setText("Max threshold = " + rightPinValue);
                break;
            case Setting.SOBEL_MODE:
                sobelThreshold.setText("Sobel threshold = " + rightPinValue);
                break;
            default:
                Toast.makeText(SettingActivity.this, "Error in class " + SettingActivity.class.getName(), Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
