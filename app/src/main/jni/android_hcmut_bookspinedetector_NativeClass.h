/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class android_hcmut_bookspinedetector_NativeClass */

#ifndef _Included_android_hcmut_bookspinedetector_NativeClass
#define _Included_android_hcmut_bookspinedetector_NativeClass
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     android_hcmut_bookspinedetector_NativeClass
 * Method:    getStringFromNative
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_android_hcmut_bookspinedetector_NativeClass_getStringFromNative
  (JNIEnv *, jclass);

#ifdef __cplusplus
}
#endif
#endif
